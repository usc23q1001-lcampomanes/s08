from django.db import models

# Create your models here.

class GroceryItem(models.Model):
	task_name= models.CharField(max_length=50)
	category = models.CharField(max_length=50)
	status = models.CharField(max_length=50, default="Pending")
	date_created=models.DateTimeField('Date Created')
	