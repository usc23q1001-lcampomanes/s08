
from abc import ABC, abstractclassmethod

class Animal(ABC):

	@abstractclassmethod
	def eat(self, food):
		pass

	def make_sound():
		pass

class Cat(Animal):

	def __init__(self,Name,Breed,Age):
		self.Name = Name
		self.Breed = Breed
		self.Age = Age

	def set_Name(self, Name):
		self.Name = Name

	def get_Name(self):
		print(f'My Name is {self.Name}') 

	def set_Breed(self, Breed):
		self.Breed = Breed

	def get_Breed(self):
		print(f'My Breed is {self.Breed}') 

	def set_Age(self, Age):
		self.Breed = Age

	def get_Age(self):
		print(f'My Age is {self.Age}') 

	def eat(self, food):
		print(f'Im eating {food}')

	def make_sound(self):
		print('Meow! Meow!')

class Dog(Animal):

	def __init__(self,Name,Breed,Age):
		self.Name = Name
		self.Breed = Breed
		self.Age = Age

	def set_Name(self, Name):
		self.Name = Name

	def get_Name(self):
		print(f'My Name is {self.Name}') 

	def set_Breed(self, Breed):
		self.Breed = Breed

	def get_Breed(self):
		print(f'My Breed is {self.Breed}') 

	def set_Age(self, Age):
		self.Breed = Age

	def get_Age(self):
		print(f'My Age is {self.Age}') 

	def eat(self, food):
		print(f'Im eating {food}')

	def make_sound(self):
		print('Arf! Arf!')



cat = Cat('Lily','White Ragdoll', 3)
cat.get_Name()
cat.get_Breed()
cat.get_Age()
cat.eat("Tuna")
cat.make_sound()

dog = Dog('Love','Doberman', 4)
dog.get_Name()
dog.get_Breed()
dog.get_Age()
dog.eat("Meat")
dog.make_sound()