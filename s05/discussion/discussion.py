# [SECTION] Python Class Review

class SampleClass():
	def __init__(self, year):
		self.year = year

	def show_year(self):
		print(f'The year is: {self.year}')

myObj = SampleClass(2020)

# print(myObj.year)
# myObj.show_year()

# [SECTION] Fundamentals of OOP
#There are four main fundamental principles in OOP
# Encapsulation
# Inheritance
# Polymorphism
# Abstraction

# [SECTION] Encapsulation

class Person():
	def __init__(self):
		self._name = "Kylie"
		self._age = 23

		#setter
	def set_name(self, name):
		self._name= name

	def get_name(self):
		print(f'Name of Person {self._name}')

	def set_age(self, age):
		self._age= age

	def get_age(self):
		print(f'Age of Person {self._age}')

# p1=Person()
# p1.get_age()
# p1.set_age(24)
# p1.get_age()

# [SECTION] Inheritance

class Employee(Person):
	def __init__ (self, employeeId):
		
		super().__init__()
		self._employeeId= employeeId

	def set_employeeId(self, employeeId):
		self._employeeId= employeeId

	def get_employeeId(self):
		print(f'The Employee ID is {self._employeeId}')

	def get_details(self):
		print(f"{self._employeeId} belongs to {self._name}")

# emp1 = Employee("Emp-001")
# emp1.get_details()
# emp1.get_name()
# emp1.get_age()
# emp1.set_name("Marie")
# emp1.set_employeeId("Emp-011")
# emp1.get_details()

class Student(Person):
	def __init__ (self, studentNo, course, yearLevel):
		
		super().__init__()
		self._studentNo= studentNo
		self._course= course
		self._yearLevel= yearLevel

	def set_studentNo(self, studentNo):
		self._studentNo= studentNo

	def get_studentNo(self):
		print(f'The Student Number is {self._studentNo}')

	def set_course(self, course):
		self._employeeId= course

	def get_course(self):
		print(f'Student Course is {self._course}')

	def set_yearLevel(self, yearLevel):
		self._yearLevel= yearLevel

	def get_yearLevel(self):
		print(f'Student Year Level is {self._yearLevel}')

	def get_details(self):
		print(f"{self._name} is currently in year {self._yearLevel} taking up {self._course}")

# student = Student(191,"BS IT",3)
# student.get_details()

# [POLYMORPHISM]

class Admin():
	def is_admin(self):
		print(True)

	def user_type(self):
		print('Admin User')

class Customer():
	def is_admin(self):
		print(False)

	def user_type(self):
		print('Customer User')

def test_function(obj):
	obj.is_admin()
	obj.user_type()


user_admin= Admin()
user_customer = Customer()

test_function(user_admin)
test_function(user_customer)

# def TeamLeader():
# 	def occupation(self):
# 		print("Team Leader")

# 	def hasAuth(self):
# 		print(True)

# def TeamMember():
# 	def occupation(self):
# 		print("Team Member")

# 	def hasAuth(self):
# 		print(False)

# tl1= TeamLeader()
# tm1= TeamMember()

# for person in (tl1, tm1):
# 	person.occupation()

class Zuitt():
	def tracks(self):
		print("We are currently offering 3 tracks (developer career, pi-shape career, and short courses)")

	def num_of_hours(self):
		print("Learn web development in 360 hours!")

class DeveloperCareer(Zuitt):
	def num_of_hours(self):
		print("Learn the basics of web development in 240 hours!")

class PiShapedCareer(Zuitt):
	def num_of_hours(self):
		print("Learn skills for no-code app development in 140 hours!")

class ShortCourses(Zuitt):
	def num_of_hours(self):
		print("Learn advance topics in web development in 20 hours!")

# course1 = DeveloperCareer()
# course2= PiShapedCareer()
# course3= ShortCourses()

# course1.num_of_hours()
# course2.num_of_hours()
# course3.num_of_hours()

# [SECTION] ABSTRACTION

from abc import ABC, abstractclassmethod

class Polygon(ABC):
	@abstractclassmethod
	def print_number_of_sides(self):
		pass 

class Triangle(Polygon):
	def __init__(self):
		super().__init__()

	def print_number_of_sides(self):
		print("This polygon has three sides")

class Pentagon(Polygon):
	def __init__(self):
		super().__init__()

	def print_number_of_sides(self):
		print("This polygon has five sides")

shape1 = Triangle()
shape2 = Pentagon()

shape1.print_number_of_sides()
shape2.print_number_of_sides()